# Applicator program for Android instrumentation blueprints

This repository contains the blueprint applicator program introduced in the
bachelor thesis *Composing instrumentation tools for Android apps*, by Arthur
van der Staaij. The thesis is included in this repository as well. More
information about what what instrumentation blueprints are, and what the purpose
of this program is, can be found in the included thesis.

## Building

Building this program requires a C++17 compiler.
[GNU Make](https://www.gnu.org/software/make/) is used as a build system. By
default, the GCC compiler is used, but a different C++ compiler can be specified
by changing the "CXX = " line in the Makefile.

To build the program, use:

```bash
make
```

This will generate the executable `bin/applybp`.
You can optionally install this executable by copying it to a directory in your
`PATH`.

## Usage

For general information on how to use the program, use:

```bash
./bin/applybp --help
```

The program operates on Smali files, but APK unpacking and repacking are
currently not built-in. To unpack APK's and get access to their Smali files,
[Apktool](https://ibotpeaches.github.io/Apktool/) is recommended. Apktool can
also repack the APK after blueprints have been applied to it. To install a
repacked APK, it first has to be re-signed, and it should preferably be aligned
as well. This can be done with the `apksigner` and `zipalign` tools from the
Android SDK, which comes with
[Android Studio](https://developer.android.com/studio).
Alternatively, [ACVTool](https://github.com/pilgun/acvtool) has a `sign`
command that can be used to both sign and align an APK, but this still requires
the Android SDK to be installed. ACVTool also has a `build` command that can
sign, align and build an unpacked APK in one go, but this requires Apktool.

The current prototype blueprint syntax can only represent changes to the
contents of existing Smali methods. For a procedure to circumvent this
limitation when using only a single instrumentation tool, see the section
*Evaluation* of the included thesis.
