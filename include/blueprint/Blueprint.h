#pragma once

#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#include "smali/MethodLine.h"

namespace blueprint {

//==================================================================================================
// Data type
//==================================================================================================

/// An instrumentation blueprint
struct Blueprint {
	/// A list of Smali method lines
	using MethodLineList = std::vector<smali::MethodLine>;

	enum class PatchType {
		Prepend,
		Append,
		Replace
	};

	/// A line patch; maps patch types to method line lists
	using LinePatch = std::unordered_map<PatchType, MethodLineList>;

	/// Maps smali line numbers to line patches
	using LinePatchMap = std::map<unsigned, LinePatch>;

	/// A method entry, excluding the method descriptor
	struct MethodEntry {
		unsigned instrumentationRegisterCount;
		LinePatchMap linePatchMap;
	};

	/// Maps method descriptors to method entries
	using MethodEntryMap = std::map<std::string, MethodEntry>;

	MethodEntryMap methodEntryMap;

	/// Returns the string representation of this Blueprint
	std::string toString() const;
	/// Prints the string representation of this blueprint to @p outputStream
	void print(std::ostream& outputStream) const;
};

//==================================================================================================
// Parsing
//==================================================================================================

/// A blueprint parsing error
class ParseError : std::runtime_error {
public:
	enum class Type {
		ExpectedHeader,
		ExpectedMethodEntryHeader,
		InvalidMethodEntryHeader,
		InvalidLineEntryHeader,
		DoubleReplacement
	};

	ParseError() : std::runtime_error("") {}

	ParseError(Type type, uint64_t lineNumber)
		: std::runtime_error(""), type(type), lineNumber(lineNumber)
	{}

	/// The type of parse error
	Type type;
	/// Line number of the blueprint file line at which the parse error occurred
	uint64_t lineNumber;
};

/// Parses a blueprint given as a character stream.
/// @param[in, out] inputStream The blueprint as a character stream. Characters will be consumed
///                             until the stream is empty or an error occurs.
/// @returns On success, returns a parsed Blueprint structure.
/// @exception BlueprintParseError On failure, throws a BlueprintParseError with details about the
///                                error.
Blueprint parse(std::istream& inputStream);

/// Parses a blueprint given as a character stream and merges it into another.
/// In contrast to blueprint::merge, this throws a ParseError instead of a MergeError on a merge
/// failure, and thus gives access to the line number where the merge error occurred.
/// @param[in     ] blueprint   The blueprint to merge the parsed blueprint into.
///                             This is not passed by reference, because it would have to be copied
///                             anyway to achieve a strong exception guarantee. Use std::move if
///                             you don't care about the original blueprint.
/// @param[in, out] inputStream The blueprint to parse as a character stream. Characters will be
///                             consumed until the stream is empty or an error occurs.
/// @returns On success, returns a blueprint containing both the patches of @p blueprint and those
///          parsed from @p inputStream.
/// @exception ParseError On failure, throws a ParseError with details about the error.
Blueprint parseAndMerge(Blueprint blueprint, std::istream& inputStream);

//==================================================================================================
// Merging
//==================================================================================================

/// A blueprint merging error
class MergeError : std::runtime_error {
public:
	enum class Type {
		DoubleReplacement
	};

	MergeError()          : std::runtime_error("") {}
	MergeError(Type type) : std::runtime_error(""), type(type) {}

	/// The type of merge error
	Type type;
};

/// Attempts to merge the given blueprints.
/// @returns On success, returns a blueprint containing the patches of both given blueprints.
/// @exception MergeError On failure, throws a MergeError with details about the error.
Blueprint merge(const Blueprint& first, const Blueprint& second);

//==================================================================================================
// Printing
//==================================================================================================

/// Prints @p blueprint to @p outputStream.
void print(const Blueprint& blueprint, std::ostream& outputStream);

} // namespace blueprint
