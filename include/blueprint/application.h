#pragma once

#include <stdexcept>
#include "blueprint/Blueprint.h"

namespace blueprint {

/// A blueprint application error
class ApplicationError : std::runtime_error {
public:
	enum class Type {
		InvalidClassHeader,
		MethodWithoutClass,
		InvalidMethodHeader,
		InvalidLocalRegisterCount,
		UnclosedMethod
	};

	ApplicationError() : std::runtime_error("") {}

	ApplicationError(Type type, uint64_t lineNumber)
		: std::runtime_error(""), type(type), lineNumber(lineNumber)
	{}

	/// The type of parse error
	Type type;
	/// Line number of the input stream at which the application error occurred
	uint64_t lineNumber;
};

/// Applies the given blueprint to a given stream of smali code.
/// @param[in]      blueprint    The blueprint to apply.
/// @param[in, out] inputStream  An input stream of smali code to which the blueprint is applied.
/// @param[in, out] outputStream An output stream to which the result of the application is written.
/// @exception ApplicationError On failure, throws an ApplicationError with details about the error.
void apply(
	const Blueprint& blueprint,
	std::istream& inputStream,
	std::ostream& outputStream
);

} // namespace blueprint
