#pragma once

#include <algorithm>
#include <string>

namespace util {

/// Returns whether the specified range contains @p value.
template<typename InputIt>
bool contains(InputIt begin, InputIt end, const typename InputIt::reference value) {
	return std::find(begin, end, value) != end;
}

/// Attempts to insert an element into the given @p map, which must be an std::map.
/// If the map already contains an element with the given key, nothing is inserted, @p outIt is
/// filled with an iterator to the conflicting element, and false is returned.
/// If the map does not yet contain such an element, the passed element is inserted, @p outIt is
/// filled with an iterator to the newly inserted element, and true is returned.
template<typename Map>
bool insertOrFind(
	Map& map,
	const typename Map::key_type& key,
	const typename Map::mapped_type& value,
	typename Map::iterator& outIt
) {
	const auto& pair = map.emplace(std::make_pair(key, value));
	outIt = pair.first;
	return pair.second;
}

/// Parses a number consisting of only digits, with no preceding whitespace. If a number is parsed
/// successfully, it is stored in out_number and true is returned. Otherwise, false is returned.
template<typename T>
bool parseBasicInteger(const std::string& str, T& out_number) {
	if (str.empty()) {
		return false;
	}

	T number = 0;
	for (char c : str) {
		if (c < '0' || c > '9') {
			return false;
		}
		number = number * 10 + (c - '0');
	}

	out_number = number;
	return true;
}

} // namespace util
