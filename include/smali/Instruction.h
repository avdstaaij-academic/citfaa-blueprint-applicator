#pragma once

#include <string>
#include <vector>
#include "smali/Argument.h"

namespace smali {

/// Minimal representation of a Smali instruction
struct Instruction {
	std::string precedingWhitespace;
	std::string operation;
	std::vector<Argument> arguments;

	/// Returns the string representation of this instruction
	std::string toString() const;
};

/// Parses a line as a Smali instruction
Instruction parseInstruction(const std::string& line);

} // namespace smali
