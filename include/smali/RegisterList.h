#pragma once

#include <vector>
#include "smali/Register.h"

namespace smali {

/// Represents a brace-enclosed list of register references
struct RegisterList {
	std::vector<Register> list;

	// Whether this is a {reg .. reg} list instead of a {reg, reg, reg} list.
	// If this is true, the list field will contain only two registers: the first and the last one.
	bool isRange;

	/// Returns the string representation of this register list
	std::string toString() const;
};

/// Attempts to parse a string as a Smali register reference list. On Success, returns true and
/// fills @p outRegister with the parsed RegisterList. On failure, returns false.
bool tryParseRegisterList(const std::string& str, RegisterList& outRegisterList);

} // namespace smali
