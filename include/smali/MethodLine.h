#pragma once

#include <istream>
#include <string>
#include "smali/Instruction.h"

namespace smali {

/// Represents a line of a method
class MethodLine {
public:
	MethodLine(const Instruction& instruction) : line(instruction) {}
	MethodLine(const std::string& str = "")    : line(str)         {}

	bool isInstruction() const { return line.index() == 0; }
	bool isOther()       const { return line.index() == 1; }

	void setInstruction(const Instruction& instruction) { line = instruction; }
	void setOther      (const std::string& str)         { line = str; }

	Instruction getInstruction() const { return std::get<0>(line); }
	std::string getOther()       const { return std::get<1>(line); }

	/// Returns the string representation of this method line
	std::string toString() const;
private:
	std::variant<Instruction, std::string> line;
};

// Parses a string consisting of one more lines as Smali method lines.
// There is no singular form because this method is context-sensitive.
std::vector<MethodLine> parseMethodLines(const std::string& str);

} // namespace smali
