#pragma once

#include <string>
#include <variant>
#include "smali/Register.h"
#include "smali/RegisterList.h"

namespace smali {

/// Represents an argument in an instruction
class Argument {
public:
	Argument(const Register&     reg     ) : argument(reg    ) {}
	Argument(const RegisterList& regList ) : argument(regList) {}
	Argument(const std::string&  str = "") : argument(str    ) {}

	bool isRegister()     const { return argument.index() == 0; }
	bool isRegisterList() const { return argument.index() == 1; }
	bool isOther()        const { return argument.index() == 2; }

	void setRegister    (const Register& reg)         { argument = reg; }
	void setRegisterList(const RegisterList& regList) { argument = regList; }
	void setOther       (const std::string& str)      { argument = str; }

	Register     getRegister()     const { return std::get<0>(argument); }
	RegisterList getRegisterList() const { return std::get<1>(argument); }
	std::string  getOther()        const { return std::get<2>(argument); }

	/// Returns the string representation of this argument
	std::string toString() const;
private:
	std::variant<Register, RegisterList, std::string> argument;
};

/// Parses a string as a Smali instruction argument
Argument parseArgument(const std::string& str);

} // namespace smali
