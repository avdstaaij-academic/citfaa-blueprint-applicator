#pragma once

#include <cstdint>
#include <string>

namespace smali {

/// Represents a register reference
struct Register {
	enum class Type {
		Local,
		Parameter,
		Instrumentation
	};
	using number_t = uint16_t;
	Type type;
	number_t number;

	/// Returns the string representation of this register
	std::string toString() const;
};

/// Attempts to parse a string as a Smali register reference. On Success, returns true and fills
/// @p outRegister with the parsed Register. On failure, returns false.
bool tryParseRegister(const std::string& str, Register& outRegister);

/// Returns the prefix character corresponding to the specified register type, or '\0' for an
/// invalid register type
char registerTypeToPrefix(Register::Type type);

/// Returns the register type corresponding to the specified prefix character, or -1 if the
/// specified character does not match any type
Register::Type registerPrefixToType(char prefix);

} // namespace smali
