#include "blueprint/Blueprint.h"

#include <cstdint>
#include <sstream>
#include "util.h"

namespace blueprint {

//==================================================================================================
// Constants
//==================================================================================================

/// Maps patch type strings to patch types
static const std::map<std::string, Blueprint::PatchType> STRING_TO_PATCH_TYPE_MAP = {
	{"p", Blueprint::PatchType::Prepend},
	{"a", Blueprint::PatchType::Append},
	{"r", Blueprint::PatchType::Replace},
};

/// Maps patch type strings to patch types
static const std::map<Blueprint::PatchType, std::string> PATCH_TYPE_TO_STRING_MAP = {
	{Blueprint::PatchType::Prepend, "p"},
	{Blueprint::PatchType::Append,  "a"},
	{Blueprint::PatchType::Replace, "r"},
};

//==================================================================================================
// Helper types
//==================================================================================================

/// A full method entry, including the method descriptor
struct FullMethodEntry {
	std::string methodDescriptor;
	Blueprint::MethodEntry methodEntry;
};

/// A full line entry, including the line number patch type
struct FullLineEntry {
	unsigned lineNumber;
	Blueprint::PatchType patchType;
	Blueprint::MethodLineList methodLineList;
};

//==================================================================================================
// Helper function declarations
//==================================================================================================

/// Parses a method entry.
/// @param[in, out] inputStream         The inputStream containing the method entry, with the header
///                                     line already extracted. Lines will be consumed up to and
///                                     including the next method entry header.
/// @param[in, out] line                The method entry header corresponding to the method entry to
///                                     be read from @p inputStream. Will be set to the next method
///                                     entry header (which is extracted from the stream).
/// @param[in, out] blueprintLineNumber The global line number of the method entry header @p line.
///                                     Used for error reporting. Will be set to the line number
///                                     of the next method entry header (which is put in @p line).
/// @returns The method entry parsed from the stream.
/// @exception BlueprintParseError On failure, throws a BlueprintParseError with details about the
///                                error.
static FullMethodEntry parseMethodEntry(
	std::istream& inputStream,
	std::string& line,
	uint64_t& blueprintLineNumber
);

/// Parses a line entry.
/// @param[in, out] inputStream         The inputStream containing the line entry, with the header
///                                     line already extracted. Lines will be consumed up to and
///                                     including the next method or line entry header.
/// @param[in, out] line                The line entry header corresponding to the line entry to be
///                                     read from @p inputStream. Will be set to the next method or
///                                     line entry header (which is extracted from the stream).
/// @param[in, out] blueprintLineNumber The global line number of the method entry header @p line.
///                                     Used for error reporting. Will be set to the line number
///                                     of the next method or line entry header (which is put in
///                                     @p line).
/// @param[    out] outLineNumber       The line entry line number parsed from the stream.
/// @param[    out] outType             The line entry patch type parsed from the stream.
/// @param[    out] outMethodLineList   The line entry MethodLineList parsed from the stream.
/// @exception BlueprintParseError On failure, throws a BlueprintParseError with details about the
///                                error.
static FullLineEntry parseLineEntry(
	std::istream& inputStream,
	std::string& line,
	uint64_t& blueprintLineNumber
);

/// Returns whether the given line is a method or line entry header.
/// The line must not be empty.
static bool isHeader(const std::string& line);

/// Returns whether the given header line is a method entry header.
/// The line must be a header, as specified by isHeader().
static bool headerIsMethodEntryHeader(const std::string& line);

/// Discards empty lines from the given stream, putting the first non-empty line in @p outLine.
/// If the stream contains no non-empty line, @p outLine is set to an empty string.
/// @returns The amount of empty lines discarded.
static uint64_t discardEmptyLines(std::istream& inputStream, std::string& outLine);

/// Inserts the given FullMethodEntry into the given MethodEntryMap, or merges it in if the given
/// MethodEntryMap already contains an entry with the given FullMethodEntry's method descriptor.
static void insertOrMergeMethodEntry(
	Blueprint::MethodEntryMap& methodEntryMap,
	const FullMethodEntry& fullMethodEntry
);

/// Inserts the given FullLineEntry into the given LinePatchMap, or merges it in if the given
/// LinePatchMap already contains an entry with the given FullLineEntry's line number.
/// Essentially a special case of insertOrMergeLinePatch.
static void insertOrMergeLineEntry(
	Blueprint::LinePatchMap& linePatchMap,
	const FullLineEntry& fullLineEntry
);

/// Inserts the given (lineNumber, linePatch)-pair into the given LinePatchMap, or merges it in if
/// the given LinePatchMap already contains an entry with the given line number.
static void insertOrMergeLinePatch(
	Blueprint::LinePatchMap& linePatchMap,
	unsigned lineNumber,
	const Blueprint::LinePatch& linePatch
);

/// Inserts the given (PatchType, MethodLineList)-pair into the given LinePatch, or merges it in if
/// the given LinePatch already contains an entry with the given PatchType.
static void insertOrMergeMethodLineList(
	Blueprint::LinePatch& linePatch,
	Blueprint::PatchType patchType,
	const Blueprint::MethodLineList& methodLineList
);

//==================================================================================================
// Implementation
//==================================================================================================

Blueprint parse(std::istream& inputStream) {
	Blueprint blueprint;
	return parseAndMerge(blueprint, inputStream);
}

Blueprint parseAndMerge(Blueprint blueprint, std::istream& inputStream) {
	uint64_t blueprintLineNumber = 0;
	std::string line;

	// Discard leading empty lines
	blueprintLineNumber += discardEmptyLines(inputStream, line);

	// If the stream contained nothing but empty lines
	if (inputStream.eof()) {
		return blueprint;
	}

	// The first non-empty line should be a method entry header
	if (!(isHeader(line) && headerIsMethodEntryHeader(line))) {
		throw ParseError(ParseError::Type::ExpectedMethodEntryHeader, blueprintLineNumber);
	}

	do {
		auto fullMethodEntry = parseMethodEntry(inputStream, line, blueprintLineNumber);
		try {
			insertOrMergeMethodEntry(blueprint.methodEntryMap, fullMethodEntry);
		}
		catch (const MergeError& error) {
			ParseError::Type type;
			switch (error.type) {
			case MergeError::Type::DoubleReplacement:
				type = ParseError::Type::DoubleReplacement;
				break;
			}
			throw ParseError(type, blueprintLineNumber);
		}
	} while (inputStream.good());

	return blueprint;
}

static FullMethodEntry parseMethodEntry(
	std::istream& inputStream,
	std::string& line,
	uint64_t& blueprintLineNumber
) {
	// @@<method descriptor>:<instrumentation register count>

	auto headerSeparatorPos = line.rfind(':');

	if (headerSeparatorPos == std::string::npos) {
		throw ParseError(ParseError::Type::InvalidMethodEntryHeader, blueprintLineNumber);
	}

	FullMethodEntry fullMethodEntry;

	fullMethodEntry.methodDescriptor = line.substr(2, headerSeparatorPos - 2);

	try {
		fullMethodEntry.methodEntry.instrumentationRegisterCount =
			std::stoul(line.substr(headerSeparatorPos + 1));
	}
	catch (const std::logic_error&) {
		throw ParseError(ParseError::Type::InvalidMethodEntryHeader, blueprintLineNumber);
	}

	// Parse line entries

	// Discard empty lines
	blueprintLineNumber += discardEmptyLines(inputStream, line) + 1;

	// If the stream contained nothing more besides empty lines
	if (inputStream.eof()) {
		return fullMethodEntry;
	}

	// The first non-empty line should be some kind of header
	if (!(isHeader(line))) {
		throw ParseError(ParseError::Type::ExpectedHeader, blueprintLineNumber);
	}

	// If the first header is another method entry header, so if there are no line entries
	if (headerIsMethodEntryHeader(line)) {
		return fullMethodEntry;
	}

	do {
		auto fullLineEntry = parseLineEntry(inputStream, line, blueprintLineNumber);
		try {
			insertOrMergeLineEntry(fullMethodEntry.methodEntry.linePatchMap, fullLineEntry);
		}
		catch (const MergeError& error) {
			ParseError::Type type;
			switch (error.type) {
			case MergeError::Type::DoubleReplacement:
				type = ParseError::Type::DoubleReplacement;
				break;
			}
			throw ParseError(type, blueprintLineNumber);
		}
	} while (inputStream.good() && !headerIsMethodEntryHeader(line));

	return fullMethodEntry;
}

static FullLineEntry parseLineEntry(
	std::istream& inputStream,
	std::string& line,
	uint64_t& blueprintLineNumber
) {
	// @<line number>:<type>

	auto headerSeparatorPos = line.rfind(':');

	if (headerSeparatorPos == std::string::npos) {
		throw ParseError(ParseError::Type::InvalidLineEntryHeader, blueprintLineNumber);
	}

	FullLineEntry fullLineEntry;

	try {
		fullLineEntry.lineNumber = std::stoul(line.substr(1, headerSeparatorPos - 1));
	}
	catch (const std::logic_error&) {
		throw ParseError(ParseError::Type::InvalidLineEntryHeader, blueprintLineNumber);
	}

	std::string patchTypeString = line.substr(headerSeparatorPos + 1);
	auto it = STRING_TO_PATCH_TYPE_MAP.find(patchTypeString);
	if (it == STRING_TO_PATCH_TYPE_MAP.end()) {
		throw ParseError(ParseError::Type::InvalidLineEntryHeader, blueprintLineNumber);
	}
	fullLineEntry.patchType = it->second;

	// Parse method line list

	std::string methodLines = "";
	while (std::getline(inputStream, line).good() && !isHeader(line)) {
		++blueprintLineNumber;
		methodLines += line + "\n";
		fullLineEntry.methodLineList = smali::parseMethodLines(methodLines);
	}

	return fullLineEntry;
}

static bool isHeader(const std::string& line) {
	return line.front() == '@';
}

static bool headerIsMethodEntryHeader(const std::string& line) {
	return line.length() >= 2 && line[1] == '@';
}

static uint64_t discardEmptyLines(std::istream& inputStream, std::string& outLine) {
	outLine = "";
	uint64_t count = 0;
	while (std::getline(inputStream, outLine).good() && outLine.empty()) {
		++count;
	}
	return count;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

Blueprint merge(const Blueprint& first, const Blueprint& second) {
	Blueprint result = first;
	for (const auto& entry : second.methodEntryMap) {
		insertOrMergeMethodEntry(result.methodEntryMap, {entry.first, entry.second});
	}
	return result;
}

static void insertOrMergeMethodEntry(
	Blueprint::MethodEntryMap& methodEntryMap,
	const FullMethodEntry& fullMethodEntry
) {
	Blueprint::MethodEntryMap::iterator it;
	if (util::insertOrFind(
		methodEntryMap, fullMethodEntry.methodDescriptor, fullMethodEntry.methodEntry, it
	)) {
		// The FullMethodEntry was successfully inserted
		return;
	}
	// Merge the FullMethodEntry in
	auto& existingMethodEntry = it->second;
	// Loop with copies, since we have to change them
	for (auto entry : fullMethodEntry.methodEntry.linePatchMap) {
		// Increment register numbers for the line patch that is being merged in
		for (auto& entry : entry.second) {
			for (auto& methodLine : entry.second) {
				if (methodLine.isInstruction()) {
					auto instruction = methodLine.getInstruction();
					for (auto& argument : instruction.arguments) {
						if (argument.isRegister()) {
							auto reg = argument.getRegister();
							if (reg.type == smali::Register::Type::Instrumentation) {
								reg.number += existingMethodEntry.instrumentationRegisterCount;
								argument.setRegister(reg);
							}
						}
						else if (argument.isRegisterList()) {
							auto regList = argument.getRegisterList();
							for (auto& reg : regList.list) {
								if (reg.type == smali::Register::Type::Instrumentation) {
									reg.number += existingMethodEntry.instrumentationRegisterCount;
								}
							}
							argument.setRegisterList(regList);
						}
					}
					methodLine.setInstruction(instruction);
				}
			}
		}
		// Merge in the line patch
		insertOrMergeLinePatch(existingMethodEntry.linePatchMap, entry.first, entry.second);
	}
	// Update the instrumentation register count
	existingMethodEntry.instrumentationRegisterCount +=
		fullMethodEntry.methodEntry.instrumentationRegisterCount;
}

static void insertOrMergeLineEntry(
	Blueprint::LinePatchMap& linePatchMap,
	const FullLineEntry& fullLineEntry
) {
	insertOrMergeLinePatch(
		linePatchMap,
		fullLineEntry.lineNumber,
		Blueprint::LinePatch{{fullLineEntry.patchType, fullLineEntry.methodLineList}}
	);
}

static void insertOrMergeLinePatch(
	Blueprint::LinePatchMap& linePatchMap,
	unsigned lineNumber,
	const Blueprint::LinePatch& linePatch
) {
	Blueprint::LinePatchMap::iterator it;
	if (util::insertOrFind(linePatchMap, lineNumber, linePatch, it)) {
		// The LinePatch was successfully inserted
		return;
	}
	// Merge the LinePatch in
	auto& existingLinePatch = it->second;
	for (const auto& entry : linePatch) {
		insertOrMergeMethodLineList(existingLinePatch, entry.first, entry.second);
	}
}

static void insertOrMergeMethodLineList(
	Blueprint::LinePatch& linePatch,
	Blueprint::PatchType patchType,
	const Blueprint::MethodLineList& methodLineList
) {
	Blueprint::LinePatch::iterator it;
	if (util::insertOrFind(linePatch, patchType, methodLineList, it)) {
		// The MethodLineList was successfully inserted
		return;
	}
	// Merge the method line list list in
	if (it->first == Blueprint::PatchType::Replace) {
		// It's not possible to merge two line replacements
		throw MergeError(MergeError::Type::DoubleReplacement);
	}
	auto& existingMethodLineList = it->second;
	existingMethodLineList.insert(
		existingMethodLineList.end(),
		methodLineList.begin(),
		methodLineList.end()
	);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

std::string Blueprint::toString() const {
	std::ostringstream ss;
	print(ss);
	return ss.str();
}

void Blueprint::print(std::ostream& outputStream) const {
	for (const auto& methodEntryMapEntry : methodEntryMap) {
		outputStream << "@@" << methodEntryMapEntry.first.c_str()
		             << ":" << methodEntryMapEntry.second.instrumentationRegisterCount << "\n";
		for (const auto& linePatchMapEntry : methodEntryMapEntry.second.linePatchMap) {
			for (const auto& linePatchEntry : linePatchMapEntry.second) {
				outputStream << "@" << linePatchMapEntry.first
				             << ":" << PATCH_TYPE_TO_STRING_MAP.at(linePatchEntry.first) << "\n";
				for (const auto& methodLine : linePatchEntry.second) {
					outputStream << methodLine.toString() << "\n";
				}
			}
		}
	}
}

} // namespace blueprint
