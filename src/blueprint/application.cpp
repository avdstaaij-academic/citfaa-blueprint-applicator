#include "blueprint/application.h"

#include <map>
#include <vector>

#include "util.h"

namespace blueprint {

//==================================================================================================
// Helper types
//==================================================================================================

/// This is a simplification of the available types.
/// We only need to know which move instruction to use.
/// Longs and doubles are Wide type, objects and arrays are Object type, and the rest is Normal.
enum class SmaliType {
	Normal,
	Wide,
	Object
};

/// The information about a method that is required for blueprint application
struct MethodInfo {
	std::string methodDescriptor;      // Only the local part; excludes the class name
	std::vector<SmaliType> parameters; // Includes the this-parameter, if the method has one
};

//==================================================================================================
// Constants
//==================================================================================================

// String which is prepended to the Smali code in the blueprint
static const std::string INDENT_STRING = "    ";

static const std::map<SmaliType, std::string> SMALI_TYPE_TO_COPY_INSTRUCTION_MAP = {
	{SmaliType::Normal, "move/16"       },
	{SmaliType::Wide,   "move-wide/16"  },
	{SmaliType::Object, "move-object/16"}
};

//==================================================================================================
// Helper function declarations
//==================================================================================================

/// Parses the class descriptor from a class header.
/// @param[in] classHeader The class header to parse.
/// @param[in] lineNumber  The line number of @p classHeader. Used for error reporting.
/// @returns The class descriptor parsed from the class header
static std::string parseClassHeader(const std::string classHeader, uint64_t lineNumber);

/// Applies the given blueprint to a Smali method.
/// @param[in     ] blueprint       The blueprint to apply.
/// @param[in, out] inputStream     An input stream of smali code that currently points the line
///                                 after the header of a method, to which the blueprint is applied.
///                                 After the function call, the stream will point to the first line
///                                 after the ".end method" line.
/// @param[in     ] methodHeader    The header of the method to which the blueprint is applied.
/// @param[in     ] classDescriptor The descriptor of the class to which the method belongs.
/// @param[in, out] outputStream    An output stream to which the result of the application is
///                                 written
/// @param[in, out] lineNumber      The line number of the method header @p methodHeader. Used for
///                                 error reporting. Will be set to the line number of the last line
///                                 that was extracted from @p inputStream after the call.
static void applyToMethod(
	const Blueprint& blueprint,
	std::istream& inputStream,
	const std::string& methodHeader,
	const std::string& classDescriptor,
	std::ostream& outputStream,
	uint64_t& lineNumber
);

/// Parses a method header.
/// @param[in] methodHeader The method header to parse.
/// @param[in] lineNumber   The line number of @p methodHeader. Used for error reporting.
/// @returns The information parsed from the method header.
static MethodInfo parseMethodHeader(const std::string& methodHeader, uint64_t lineNumber);

/// Parses the ".locals"/".registers" line at the beginning of a method, if one exists.
/// @param[in ] line                  The line after a method header that may contain ".locals" or
///                                   ".registers".
/// @param[in ] methodInfo            The information on the method header preceding @p line
/// @param[in ] lineNumber            The line number of @p line. Used for error reporting.
/// @param[out] outLocalRegisterCount If @p line is a ".locals" or ".registers" line, this argument
///                                   is filled with the number of local registers it specifies.
/// @returns Whether @p line is a ".locals" or ".registers" line.
static bool parseLocalRegisterCount(
	const std::string& line,
	const MethodInfo& methodInfo,
	uint64_t lineNumber,
	unsigned& outLocalRegisterCount
);

/// Throws a BlueprintApplicationError with the specified type and line number if @p pos equals
/// std::string::npos
static void throwIfNpos(
	std::string::size_type pos,
	ApplicationError::Type type,
	uint64_t lineNumber
);

/// Returns whether the given line is a Smali class header
static bool isClassHeader(const std::string& line);

/// Returns whether the given line is a Smali method header
static bool isMethodBegin(const std::string& line);

/// Returns whether the given line is a Smali method terminator
static bool isMethodEnd(const std::string& line);

/// Returns whether the given other-line is a line that cannot receive a blueprint patch
static bool isNonPatchableLine(const std::string& line);

// Returns whether the given line contains some kind of debug information. It should have no
// preceding whitespace.
static bool isDebugLine(const std::string& line);

/// Returns the string representation of @p methodLine, adjusting any contained parameter and/or
/// instrumentation registers according to @p absoluteFirstParameterIndex (the v-index of the first
/// parameter register) and @p absoluteFirstInstrumentationRegisterIndex.
static std::string methodLineToAdjustedString(
	const smali::MethodLine& methodLine,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
);

/// Returns the string representation of @p instruction, adjusting any contained parameter and/or
/// instrumentation registers according to @p absoluteFirstParameterIndex (the v-index of the first
/// parameter register) and @p absoluteFirstInstrumentationRegisterIndex.
static std::string instructionToAdjustedString(
	smali::Instruction instruction,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
);

/// Converts @p reg into a v-register, according to @p absoluteFirstParameterIndex (the v-index of
/// the first parameter register) and @p absoluteFirstInstrumentationRegisterIndex.
static void adjustRegister(
	smali::Register& reg,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
);

//==================================================================================================
// Implementation
//==================================================================================================

void apply(
	const Blueprint& blueprint,
	std::istream& inputStream,
	std::ostream& outputStream
) {
	// Should be the number of the line before the first.
	// Since we count line numbers from 1, this is 0.
	uint64_t lineNumber = 0;

	std::string currentClassDescriptor = "";

	std::string line;
	while (std::getline(inputStream, line)) {
		++lineNumber;
		if (isMethodBegin(line)) {
			if (currentClassDescriptor == "") {
				throw ApplicationError(ApplicationError::Type::MethodWithoutClass, lineNumber);
			}
			applyToMethod(
				blueprint, inputStream, line, currentClassDescriptor, outputStream, lineNumber
			);
			continue;
		}
		if (isClassHeader(line)) {
			currentClassDescriptor = parseClassHeader(line, lineNumber);
		}
		// If we're not in a method, just copy the line to the output
		outputStream << line << "\n";
	}
}

static std::string parseClassHeader(const std::string classHeader, uint64_t lineNumber) {
	// I'm fairly sure the class descriptor cannot contain spaces, so this should do to isolate it
	auto descriptorBegin = classHeader.rfind(' ');
	throwIfNpos(descriptorBegin, ApplicationError::Type::InvalidClassHeader, lineNumber);
	return classHeader.substr(descriptorBegin+1);
}

static void applyToMethod(
	const Blueprint& blueprint,
	std::istream& inputStream,
	const std::string& methodHeader,
	const std::string& classDescriptor,
	std::ostream& outputStream,
	uint64_t& lineNumber
) {
	outputStream << methodHeader << "\n";

	auto methodInfo = parseMethodHeader(methodHeader, lineNumber);
	auto fullMethodDescriptor = classDescriptor + "->" + methodInfo.methodDescriptor;
	auto methodEntryIt = blueprint.methodEntryMap.find(fullMethodDescriptor);

	std::string line;

	// If the method is not in the blueprint, we act as if we're not even in a method, just copying
	// lines
	if (methodEntryIt == blueprint.methodEntryMap.end()) {
		while (true) {
			if (!inputStream.good()) {
				throw ApplicationError(ApplicationError::Type::UnclosedMethod, lineNumber);
			}
			++lineNumber;
			std::getline(inputStream, line);
			outputStream << line << "\n";
			if (isMethodEnd(line)) break;
		}
		return;
	}

	const auto& methodEntry = methodEntryIt->second;

	// Modify the .locals/.registers line, if it is present.
	// If there is no such line, we're not adding one either: it probably means that the method is
	// either abstract or native.
	if (!inputStream.good()) {
		throw ApplicationError(ApplicationError::Type::UnclosedMethod, lineNumber);
	}
	++lineNumber;
	std::getline(inputStream, line);
	unsigned localRegisterCount = 0;
	if (parseLocalRegisterCount(line, methodInfo, lineNumber, localRegisterCount)) {
		// TODO: maybe ensure format flags are good
		outputStream << INDENT_STRING << ".locals "
		             << localRegisterCount + methodEntry.instrumentationRegisterCount << "\n";

		// Write code to copy parameter registers to their original absolute positions
		if (!methodInfo.parameters.empty() && methodEntry.instrumentationRegisterCount != 0) {
			outputStream << "\n"; // Add a newline after the .locals line for readability
			unsigned parameterIndex = 0;
			for (auto parameterType : methodInfo.parameters) {
				auto copyInstruction = SMALI_TYPE_TO_COPY_INSTRUCTION_MAP.at(parameterType);
				// TODO ensure stream settings are good
				outputStream << INDENT_STRING << copyInstruction
							<< " v" << localRegisterCount + parameterIndex
							<< ", p" << parameterIndex << "\n";
				parameterIndex += (parameterType == SmaliType::Wide ? 2 : 1);
			}
		}

		if (!inputStream.good()) {
			throw ApplicationError(ApplicationError::Type::UnclosedMethod, lineNumber);
		}
		++lineNumber;
		std::getline(inputStream, line);
	}

	// Read and parse the method contents
	std::string methodLines;
	while (true) {
		if (isMethodEnd(line)) break;
		methodLines += line + "\n";
		if (!inputStream.good()) {
			throw ApplicationError(ApplicationError::Type::UnclosedMethod, lineNumber);
		}
		++lineNumber;
		std::getline(inputStream, line);
	}
	auto methodLineList = smali::parseMethodLines(methodLines);

	auto totalRegisterCount = localRegisterCount;
	for (const auto& parameter : methodInfo.parameters) {
		totalRegisterCount += (parameter == SmaliType::Wide ? 2 : 1);
	}

	// Wrapper around blueprint::methodLineToAdjustedString
	auto methodLineToAdjustedString = [=](const smali::MethodLine& methodLine){
		return blueprint::methodLineToAdjustedString(
			methodLine, localRegisterCount, totalRegisterCount
		);
	};

	// Apply the blueprint to the method contents
	unsigned lineIndex = -1;
	for (const auto& methodLine : methodLineList) {
		++lineIndex;
		// If it's a non-patchable line, just output it as-is
		if (methodLine.isOther() && isNonPatchableLine(methodLine.getOther())) {
			outputStream << methodLine.getOther() << "\n";
			// Non-patchable lines do not count as lines for the purpose of blueprint line numbers
			--lineIndex;
			continue;
		}
		// It's not a debug line
		auto linePatchIt = methodEntry.linePatchMap.find(lineIndex);
		if (linePatchIt == methodEntry.linePatchMap.end()) {
			// No line patch: just output the instruction
			outputStream << methodLineToAdjustedString(methodLine) << "\n";
			continue;
		}
		// Prepend
		auto it = linePatchIt->second.find(Blueprint::PatchType::Prepend);
		if (it != linePatchIt->second.end()) {
			for (const auto& methodLine : it->second) {
				outputStream << INDENT_STRING << methodLineToAdjustedString(methodLine) << "\n";
			}
		}
		// Replace
		it = linePatchIt->second.find(Blueprint::PatchType::Replace);
		if (it != linePatchIt->second.end()) {
			for (const auto& methodLine : it->second) {
				outputStream << INDENT_STRING << methodLineToAdjustedString(methodLine) << "\n";
			}
		}
		else {
			outputStream << methodLineToAdjustedString(methodLine) << "\n";
		}
		// Append
		it = linePatchIt->second.find(Blueprint::PatchType::Append);
		if (it != linePatchIt->second.end()) {
			for (const auto& methodLine : it->second) {
				outputStream << INDENT_STRING << methodLineToAdjustedString(methodLine) << "\n";
			}
		}
	}

	// Output the final ".end method" line
	outputStream << line << "\n";
}

static MethodInfo parseMethodHeader(
	const std::string& methodHeader,
	uint64_t lineNumber
) {
	// Shortcut for blueprint::throwIfNpos
	auto throwIfNpos = [&](const std::string::size_type pos, ApplicationError::Type type) {
		blueprint::throwIfNpos(pos, type, lineNumber);
	};

	MethodInfo methodInfo;

	// I'm fairly sure the method descriptor cannot contain spaces, so this should do to isolate it
	auto descriptorBegin = methodHeader.rfind(' ');
	throwIfNpos(descriptorBegin, ApplicationError::Type::InvalidMethodHeader);
	methodInfo.methodDescriptor = methodHeader.substr(descriptorBegin+1);
	const auto& descriptor = methodInfo.methodDescriptor;

	// If the method is not static, it has an additional "this"-parameter, which is of object type
	if (methodHeader.rfind("static", descriptorBegin-1) == std::string::npos) {
		methodInfo.parameters.push_back(SmaliType::Object);
	}

	// Parse the types of the parameters

	auto parametersBegin = descriptor.find('(');
	auto parametersEnd   = descriptor.rfind(')');
	throwIfNpos(parametersBegin, ApplicationError::Type::InvalidMethodHeader);
	throwIfNpos(parametersEnd,   ApplicationError::Type::InvalidMethodHeader);

	auto parameterString =
		descriptor.substr(parametersBegin+1, parametersEnd - (parametersBegin+1));

	unsigned parameterPos = 0u;
	while (parameterPos < parameterString.size()) {
		SmaliType type;
		switch (parameterString[parameterPos]) {
		case 'B': // Byte
		case 'C': // Char
		case 'F': // Float
		case 'I': // Int
		case 'S': // Short
		case 'Z': // Boolean
			type = SmaliType::Normal;
			++parameterPos;
			break;
		case 'D': // Double
		case 'J': // Long
			type = SmaliType::Wide;
			++parameterPos;
			break;
		case '[': // Array
			type = SmaliType::Object; // Arrays use the object move instructions
			parameterPos = parameterString.find_first_not_of('[', parameterPos+1);
			throwIfNpos(parameterPos, ApplicationError::Type::InvalidMethodHeader);
			// Fall through if the type is an object. It's hacky, but it works!
			if (parameterString[parameterPos] != 'L') {
				++parameterPos;
				break;
			}
		[[fallthrough]];
		case 'L': // Object
			type = SmaliType::Object;
			parameterPos = parameterString.find(';', parameterPos+1);
			throwIfNpos(parameterPos, ApplicationError::Type::InvalidMethodHeader);
			++parameterPos;
			break;
		default: // Includes 'V': Void, which is invalid as a parameter
			throw ApplicationError(ApplicationError::Type::InvalidMethodHeader, lineNumber);
		}
		methodInfo.parameters.push_back(type);
	}

	return methodInfo;
}

static bool parseLocalRegisterCount(
	const std::string& line,
	const MethodInfo& methodInfo,
	uint64_t lineNumber,
	unsigned& outLocalRegisterCount
) {
	auto pos = line.find_first_not_of(" \t");
	if (pos == std::string::npos) {
		return false;
	}

	// Whether the method uses ".registers" instead of ".locals"
	bool usesTotalRegisterCount = false;

	if (line.substr(pos, 10) == ".registers") {
		usesTotalRegisterCount = true;
		pos += 10;
	}
	else if (line.substr(pos, 7) == ".locals") {
		pos += 7;
	}
	else {
		return false;
	}

	// Parse the actual number
	if (!util::parseBasicInteger(line.substr(pos+1), outLocalRegisterCount)) {
		throw ApplicationError(ApplicationError::Type::InvalidLocalRegisterCount, lineNumber);
	}

	// If the number denoted the total number of registers, subtract the parameter count from it
	if (usesTotalRegisterCount) {
		outLocalRegisterCount -= methodInfo.parameters.size();
	}

	return true;
}

static void throwIfNpos(
	std::string::size_type pos,
	ApplicationError::Type type,
	uint64_t lineNumber
) {
	if (pos == std::string::npos) {
		throw ApplicationError(type, lineNumber);
	}
}

static bool isClassHeader(const std::string& line) {
	return line.substr(0, 6) == ".class";
}

static bool isMethodBegin(const std::string& line) {
	return line.substr(0, 7) == ".method";
}

static bool isMethodEnd(const std::string& line) {
	return line.substr(0, 11) == ".end method";
}

static bool isNonPatchableLine(const std::string& line) {
	auto pos = line.find_first_not_of(" \t");
	// Empty lines cannot be patched
	if (pos == std::string::npos) return true;
	// Debug lines cannot be patched
	return isDebugLine(line.substr(pos));
}

static bool isDebugLine(const std::string& line) {
	// This list may not be exhaustive
	return
		line.substr(0, 5) == ".line"     ||
		line.substr(0, 6) == ".local"    ||
		line.substr(0, 9) == ".prologue";
}

static std::string methodLineToAdjustedString(
	const smali::MethodLine& methodLine,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
) {
	if (methodLine.isInstruction()) {
		return instructionToAdjustedString(
			methodLine.getInstruction(),
			absoluteFirstParameterIndex,
			absoluteFirstInstrumentationRegisterIndex
		);
	}
	return methodLine.getOther();
}

// This could perhaps be optimized by essentially duplicating smali::Instruction::toString and
// adjusting the registers at the point where they are printed.
static std::string instructionToAdjustedString(
	smali::Instruction instruction,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
) {
	// Adjust registers
	for (auto& argument : instruction.arguments) {
		if (argument.isRegister()) {
			auto reg = argument.getRegister();
			adjustRegister(
				reg,
				absoluteFirstParameterIndex, absoluteFirstInstrumentationRegisterIndex
			);
			argument.setRegister(reg);
		}
		else if (argument.isRegisterList()) {
			auto regList = argument.getRegisterList();
			for (auto& reg : regList.list) {
				adjustRegister(
					reg,
					absoluteFirstParameterIndex, absoluteFirstInstrumentationRegisterIndex
				);
			}
			argument.setRegisterList(regList);
		}
	}
	// Return result
	return instruction.toString();
}

static void adjustRegister(
	smali::Register& reg,
	unsigned absoluteFirstParameterIndex,
	unsigned absoluteFirstInstrumentationRegisterIndex
) {
	if (reg.type == smali::Register::Type::Parameter) {
		reg.number += absoluteFirstParameterIndex;
	}
	else if (reg.type == smali::Register::Type::Instrumentation) {
		reg.number += absoluteFirstInstrumentationRegisterIndex;
	}
	reg.type = smali::Register::Type::Local;
}

} // namespace blueprint
