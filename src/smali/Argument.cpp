#include "smali/Argument.h"

namespace smali {

std::string Argument::toString() const {
	if (isRegister()) {
		return getRegister().toString();
	}
	if (isRegisterList()) {
		return getRegisterList().toString();
	}
	return getOther();
}

Argument parseArgument(const std::string& str) {
	Argument argument(str);

	// Skip whitespace
	auto firstPos = str.find_first_not_of(' ');

	if (firstPos == std::string::npos) { // If, besides whitespace, the string is empty
		return argument;
	}

	// Check if the argument is a register or a register list. If it's neither, leave it as a simple
	// string. Register lists have more restrictive syntax (must start with '{'), so we check for
	// that first.

	RegisterList regList;
	if (tryParseRegisterList(str.substr(firstPos), regList)) {
		argument.setRegisterList(regList);
		return argument;
	}

	Register reg;
	if (tryParseRegister(str.substr(firstPos), reg)) {
		argument.setRegister(reg);
		return argument;
	}

	return argument;
}

} // namespace smali
