#include "smali/RegisterList.h"

namespace smali {

std::string RegisterList::toString() const {
	std::string str = "{";
	if (isRange) {
		// In this case, the list field should have exactly two elements
		str += list[0].toString() + " .. " + list[1].toString();
	}
	else {
		for (auto it = list.begin(); it != list.end(); ++it) {
			if (it != list.begin()) {
				str += ", ";
			}
			str += it->toString();
		}
	}
	str += '}';
	return str;
}

bool tryParseRegisterList(const std::string& str, RegisterList& outRegisterList) {
	// Register list syntax:
	// Normal: {<reg>, <reg>, <reg>}
	// Range:  {<reg> .. <reg>}

	if (str.size() < 4) return false;
	if (str.front() != '{' || str.back() != '}') return false;

	RegisterList registerList;
	Register reg;

	// If the string contains "..", this is a range list
	auto pos = str.find("..", 1);

	if (pos != std::string::npos) {
		// Range list
		registerList.isRange = true;
		registerList.list.reserve(2);
		// Left register
		auto first = str.find_first_not_of(' ', 1);
		auto last  = str.find_last_not_of(' ', pos-1);
		if (
			first == std::string::npos ||
			last  == std::string::npos ||
			!tryParseRegister(str.substr(first, last-first+1), reg)
		) {
			return false;
		}
		registerList.list.emplace_back(reg);
		// Right register
		first = str.find_first_not_of(' ', pos+2);
		last  = str.find_last_not_of(' ', str.size()-2);
		if (
			first == std::string::npos ||
			last  == std::string::npos ||
			!tryParseRegister(str.substr(first, last-first+1), reg)
		) {
			return false;
		}
		registerList.list.emplace_back(reg);
	}
	else {
		// Normal list
		registerList.isRange = false;
		auto begin = str.find_first_not_of(' ', 1);
		while (begin != str.size() - 1) {
			auto end = str.find_first_of(" ,}", begin);
			if (end == std::string::npos) return false;
			if (!tryParseRegister(str.substr(begin, end-begin), reg)) return false;
			registerList.list.emplace_back(reg);
			begin = str.find_first_not_of(" ,", end); // This is guaranteed to hit the final '}'
		}
	}

	outRegisterList = std::move(registerList);
	return true;
}

} // namespace smali
