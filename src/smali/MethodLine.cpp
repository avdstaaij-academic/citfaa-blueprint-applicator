#include "smali/MethodLine.h"

#include <sstream>

namespace smali {

std::string MethodLine::toString() const {
	if (isInstruction()) {
		return getInstruction().toString();
	}
	return getOther();
}

std::vector<MethodLine> parseMethodLines(const std::string& str) {
	std::vector<MethodLine> methodLines;

	std::istringstream lines(str);
	std::string line;
	bool insideBlockStatement = false;

	while (std::getline(lines, line)) {
		// Skip whitespace
		auto pos = line.find_first_not_of(" \t");

		if (insideBlockStatement) {
			if (pos != std::string::npos && line.substr(pos, 4) == ".end") {
				insideBlockStatement = false;
			}
			// Anything inside a block statement is not an instruction
			methodLines.emplace_back(std::move(line));
			continue;
		}

		if (pos != std::string::npos && line[pos] != '.') {
			methodLines.emplace_back(parseInstruction(line));
			continue;
		}

		if (
			pos != std::string::npos && (
				// This list may not be exhaustive
				line.substr(pos+1, 10) == "annotation" ||
				line.substr(pos+1,  9) == "parameter" ||
				line.substr(pos+1, 10) == "array-data" ||
				line.substr(pos+1, 13) == "packed-switch" ||
				line.substr(pos+1, 13) == "sparse-switch"
			)
		) {
			insideBlockStatement = true;
		}

		methodLines.emplace_back(std::move(line));
	}

	return methodLines;
}

} // namespace smali
