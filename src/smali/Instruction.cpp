#include "smali/Instruction.h"

namespace smali {

std::string Instruction::toString() const {
	std::string str = precedingWhitespace + operation.c_str();
	if (!arguments.empty()) {
		str += ' ';
	}
	for (auto it = arguments.begin(); it != arguments.end(); ++it) {
		if (it != arguments.begin()) {
			str += ", ";
		}
		str += it->toString();
	}
	return str;
}

Instruction parseInstruction(const std::string& line) {
	Instruction instruction;

	auto begin = line.find_first_not_of(" \t");
	instruction.precedingWhitespace = line.substr(0, begin);

	auto end = line.find(' ', begin);
	instruction.operation =
		line.substr(begin, (end == std::string::npos ? std::string::npos : end - begin));

	while (end != std::string::npos) {
		auto begin = line.find_first_not_of(" ", end + 1);
		// We can't simply split on commas, because register lists may contain commas as well.
		// Essentially, we just skip over any commas enclosed in braces.
		// If we encounter a register list, the existence of the braces on either side is checked
		// again by smali::tryParseRegisterList, so there is a slight inefficiency there.
		if (line[begin] == '{') {
			end = line.find('}', begin);
			if (end != std::string::npos) {
				end = line.find(',', end);
			}
		}
		else {
			end = line.find(',', begin);
		}
		instruction.arguments.emplace_back(parseArgument(
			line.substr(begin, (end == std::string::npos ? std::string::npos : end - begin))
		));
	}

	return instruction;
}

} // namespace smali
