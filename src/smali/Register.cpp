#include "smali/Register.h"

#include "util.h"

namespace smali {

static const auto INVALID_REGISTER_TYPE = static_cast<Register::Type>(-1);

std::string Register::toString() const {
	return registerTypeToPrefix(type) + std::to_string(number);
}

bool tryParseRegister(const std::string& str, Register& outRegister) {
	// Register syntax: <type-char><number>

	if (str.size() < 2) {
		return false;
	}

	Register reg;
	reg.type = registerPrefixToType(str[0]);
	if (
		reg.type == INVALID_REGISTER_TYPE ||
		!util::parseBasicInteger(str.substr(1), reg.number)
	) {
		return false;
	}

	outRegister = reg;
	return true;
}

char registerTypeToPrefix(Register::Type type) {
	switch (type) {
	case Register::Type::Local:           return 'v';  break;
	case Register::Type::Parameter:       return 'p';  break;
	case Register::Type::Instrumentation: return 'i';  break;
	default:                              return '\0'; break;
	}
}

Register::Type registerPrefixToType(char prefix) {
	switch (prefix) {
	case 'v': return Register::Type::Local;           break;
	case 'p': return Register::Type::Parameter;       break;
	case 'i': return Register::Type::Instrumentation; break;
	default:  return INVALID_REGISTER_TYPE;           break;
	}
}

} // namespace smali
