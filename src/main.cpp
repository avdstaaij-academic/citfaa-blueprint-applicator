#include <algorithm>
#include <cstdio>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include <getopt.h>

#include "blueprint/Blueprint.h"
#include "blueprint/application.h"
#include "aecpp.h"
#include "util.h"

using namespace blueprint;

//==================================================================================================
// Constants
//==================================================================================================

const std::vector<std::string> COMMAND_STRINGS_APPLY = {"apply", "a"};
const std::vector<std::string> COMMAND_STRINGS_MERGE = {"merge", "m"};

const auto STYLE_ERROR    = aec::red    + aec::bold;
const auto STYLE_FILENAME = aec::yellow + aec::bold;

//==================================================================================================
// Globals
//==================================================================================================

std::string programName; // The name of the program
bool quiet = false;      // Whether the program is running in quiet mode

//==================================================================================================
// Helper types
//==================================================================================================

/// The information passed as global command-line arguments
struct ArgumentInfoGlobal {
	std::string command;
};

/// The information passed as arguments to the apply command
struct ArgumentInfoApply {
	bool useOutputDirectory;
	std::filesystem::path outputDirectoryPath;
	std::vector<std::filesystem::path> blueprintPaths;
	std::vector<std::filesystem::path> targetPaths;
};

/// An opened file
struct OpenedFile {
	std::fstream stream;
	std::filesystem::path path;
};

//==================================================================================================
// Declarations
//==================================================================================================

/// Prints usage information to stderr
void printUsage();

/// Parses and handles global command-line options.
/// After the call, the global variable optind will contain the argv index after the command
/// argument.
ArgumentInfoGlobal parseArgumentsGlobal(int argc, char* argv[]);

/// Parses and handles command-line options for the apply command.
/// The global variable optind should contain the argv index after the command argument.
ArgumentInfoApply  parseArgumentsApply (int argc, char* argv[]);

/// Executes the "apply" command. @p argc and @p argv should specify the arguments starting with the
/// command argument.
void applyBlueprints(int argc, char* argv[]);

/// Executes the "merge" command. @p argc and @p argv should specify the arguments starting with the
/// command argument
void mergeBlueprints(int argc, char* argv[]);

/// Attempts to parse the blueprint stored in @p blueprintFile and merge it into @p into.
Blueprint tryParseBlueprint(Blueprint into, OpenedFile& blueprintFile);

/// Attempts to apply @p blueprint to the smali code stored in @p smaliFile.
std::string tryApplyBlueprint(const Blueprint& blueprint, OpenedFile& smaliFile);

/// Opens a file
OpenedFile openFile(
	const std::filesystem::path& path, std::ios_base::openmode mode = std::ios_base::in
);

/// Returns an opened fstream for the file with the specified path
std::fstream fstreamFromPath(
	const std::filesystem::path& path, std::ios_base::openmode mode = std::ios_base::in
);

/// Attempts to create the specified directory path
void tryCreateDirectoryPath(const std::filesystem::path& path);

/// Prints usage information and exits in failure
void exitBadUsage();

//==================================================================================================
// Implementation
//==================================================================================================

void printUsage() {
	static const auto formatString =
R"raw(Usage:
%s [global options] <command> <command-specific arguments>

Global options:
  -h, --help    show this message
  -q, --quiet   do not print progress messages

The required <command-specific arguments> depend on the chosen <command>.
The following commands are available:

- apply, a
    Usage:
    ... apply [options] <blueprint filenames> <target>
    ... apply [options] <blueprint filenames> --targets <target> <target> ...

    Applies the specified blueprint files to the specified targets. Targets may
    be either filenames of smali files or directory names. In the case of a
    directory name, all files in the directory will be considered targets. If
    there is only one target, the result is printed to standard out by default.
    Otherwise, -o must be specified.

    Options:
      -o, --output-directory=<dir>   edited targets are placed in <dir>; no
                                     output is sent to standard out
- merge, m
    Usage:
    ... merge <blueprint filenames>

    Merges the specified blueprint files and prints the result to standard out.
    It is allowed to specify only one blueprint file: it will still be merged
    internally.
)raw";
	fprintf(stderr, formatString, programName.c_str());
}

ArgumentInfoGlobal parseArgumentsGlobal(int argc, char* argv[]) {
	ArgumentInfoGlobal info;

	static const option LONG_OPTIONS[] = {
		{ "help",  0, nullptr, 'h' },
		{ "quiet", 0, nullptr, 'q' },
		{ 0, 0, 0, 0 }
	};

	int opt;
	while ((opt = getopt_long(argc, argv, "+:hq", LONG_OPTIONS, nullptr)) != -1) {
		switch (opt) {
		case 'h':
			printUsage();
			exit(0);
		case 'q':
			quiet = true;
			break;
		case '?':
			std::cerr << STYLE_ERROR << "Error: " << aec::reset
			          << "invalid global option\n";
			exitBadUsage();
		case ':':
			std::cerr << STYLE_ERROR << "Error: " << aec::reset
			          << "missing global option argument\n";
			exitBadUsage();
		}
	}

	if (optind >= argc) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset << "missing command\n";
		exitBadUsage();
	}
	info.command = argv[optind];

	return info;
}

ArgumentInfoApply parseArgumentsApply(int argc, char* argv[]) {
	ArgumentInfoApply info;

	//fprintf(stderr, "%i, %s\n", optind, argv[optind]);

	static const option LONG_OPTIONS[] = {
		{ "output-directory", 1, nullptr, 'o'    },
		{ "targets",          0, nullptr, 127 + 1},
		{ 0, 0, 0, 0 }
	};

	bool readingTargets = false;

	// We want non-option arguments as well, and we care about the order. According to the man page,
	// this should be achievable by prefixing the optstring of getopt_long with "-", but this
	// doesn't seem to work. So we work around it using the "+"-prefix mode.
	while (optind < argc) {
		int opt = getopt_long(argc, argv, "+:o:\1", LONG_OPTIONS, nullptr);
		switch (opt) {
		case 'o':
			info.useOutputDirectory = true;
			info.outputDirectoryPath = optarg;
			break;
		case 127 + 1: // --targets
			readingTargets = true;
			break;
		case -1: // Non-option argument stored in argv[optind]
			if (readingTargets) {
				info.targetPaths.emplace_back(argv[optind]);
			}
			else {
				info.blueprintPaths.emplace_back(argv[optind]);
			}
			++optind;
			break;
		case '?':
			std::cerr << STYLE_ERROR << "Error: " << aec::reset
			          << "apply - invalid option\n";
			exitBadUsage();
		case ':':
			std::cerr << STYLE_ERROR << "Error: " << aec::reset
			          << "apply - missing option argument\n";
			exitBadUsage();
		}
	}

	// If no --targets was specified, the last file is the target
	if (!readingTargets && !info.blueprintPaths.empty()) {
		info.targetPaths.emplace_back(info.blueprintPaths.back());
		info.blueprintPaths.pop_back();
	}

	return info;
}

int main(int argc, char* argv[]) {
	programName = argv[0];
	if (argc < 2) {
		printUsage();
		exit(1);
	}
	auto info = parseArgumentsGlobal(argc, argv);
	if (util::contains(COMMAND_STRINGS_APPLY.begin(), COMMAND_STRINGS_APPLY.end(), info.command)) {
		applyBlueprints(argc-optind, argv+optind);
		return 0;
	}
	if (util::contains(COMMAND_STRINGS_MERGE.begin(), COMMAND_STRINGS_MERGE.end(), info.command)) {
		mergeBlueprints(argc-optind, argv+optind);
		return 0;
	}
	std::cerr << STYLE_ERROR << "Error: " << aec::reset
	          << "Invalid command \"" << argv[1] << "\"\n";
	exitBadUsage();
}

void applyBlueprints(int argc, char* argv[]) {
	optind = 1;
	auto info = parseArgumentsApply(argc, argv);

	// Open blueprint files

	if (info.blueprintPaths.empty()) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset
		          << "apply - no blueprint files specified\n";
		exitBadUsage();
	}

	std::vector<OpenedFile> blueprintFiles;
	for (const auto& path : info.blueprintPaths) {
		blueprintFiles.emplace_back(openFile(path));
	}

	// Gather target file paths

	// For a file from a user-specified directory, basePath contains the path of that directory.
	// Otherwise, basePath is the same as path.
	struct TargetFilePath {
		std::filesystem::path path;
		std::filesystem::path basePath;
	};

	std::vector<TargetFilePath> targetFilePaths;
	for (const auto& targetPath : info.targetPaths) {
		if (!std::filesystem::is_directory(targetPath)) {
			targetFilePaths.push_back(TargetFilePath{targetPath, targetPath});
			continue;
		}
		// Walk directories recursively
		try {
			for (
				auto it = std::filesystem::recursive_directory_iterator(targetPath);
				it != std::filesystem::recursive_directory_iterator();
				++it
			) {
				if (!it->is_directory()) {
					targetFilePaths.push_back(TargetFilePath{it->path(), targetPath});
				}
			}
		}
		catch (const std::filesystem::filesystem_error& error) {
			std::cerr << STYLE_ERROR << "Error: " << aec::reset
			          << "an error occurred while walking directory "
			          << STYLE_FILENAME << targetPath.string() << aec::reset << "\n";
			exit(1);
		}
	}

	if (targetFilePaths.empty()) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset << "apply - no target files\n";
		exitBadUsage();
	}

	if (targetFilePaths.size() > 1 && !info.useOutputDirectory) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset
		          << "apply - for multiple target files, an output directory is required\n";
		exitBadUsage();
	}

	// Optionally create the output directory

	if (info.useOutputDirectory) {
		tryCreateDirectoryPath(info.outputDirectoryPath);
	}

	// Process the blueprints

	Blueprint blueprint;
	for (auto& blueprintFile : blueprintFiles) {
		blueprint = tryParseBlueprint(std::move(blueprint), blueprintFile);
		blueprintFile.stream.close();
	}
	blueprintFiles.clear();

	// Apply the blueprints

	if (!quiet) {
		std::cerr << "Applying blueprint" << (info.blueprintPaths.size() > 1 ? "s" : "") << " to ";
		if (info.targetPaths.size() > 1) {
			std::cerr << "targets... ";
		}
		else {
			std::cerr << STYLE_FILENAME << info.targetPaths.front().string() << aec::reset
			          << "... ";
		}
	}

	for (const auto& targetFilePath : targetFilePaths) {
		auto targetFile = openFile(targetFilePath.path);
		auto result = tryApplyBlueprint(blueprint, targetFile);
		// Success!
		if (info.useOutputDirectory) {
			// If the output directory is a/b and the user specified a target directory c/d of which
			// the file we are currently handling is e/f (so c/d/e/f), we write the result to
			// a/b/d/e/f (no c!). That means we may need to create a/b/d/e (no c or f!) first.
			// Trust me, this makes sense.
			// If the target directory is specified with a trailing slash (c/d/), the result is
			// written to a/b/e/f (no d either!). This could be changed, but this behavior actually
			// kind of makes sense, since c/d/ can be interpreted as "the contents of d" instead of
			// d itself.
			auto outputFilename =
				info.outputDirectoryPath /
				targetFilePath.path.lexically_relative(targetFilePath.basePath.parent_path());
			tryCreateDirectoryPath(outputFilename.parent_path());
			auto outputFile = fstreamFromPath(outputFilename, std::ios_base::out);
			outputFile << result;
		}
		else {
			std::cout << result;
		}
	}

	if (!quiet) {
		std::cerr << "Done\n";
	}
}

void mergeBlueprints(int argc, char* argv[]) {
	// Open blueprint files

	if (argc < 2) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset
		          << "merge - no blueprint files specified\n";
		exitBadUsage();
	}

	std::vector<OpenedFile> blueprintFiles;
	for (unsigned i = 1; i < (unsigned)argc; ++i) {
		blueprintFiles.emplace_back(openFile(argv[i]));
	}

	// Parse and merge blueprints

	Blueprint blueprint;
	for (auto& blueprintFile : blueprintFiles) {
		blueprint = tryParseBlueprint(std::move(blueprint), blueprintFile);
	}

	// Success!
	blueprint.print(std::cout);
}

Blueprint tryParseBlueprint(Blueprint into, OpenedFile& blueprintFile) {
	if (!quiet) {
		std::cerr << "Processing "
		          << STYLE_FILENAME << blueprintFile.path.string() << aec::reset << "... ";
	}

	Blueprint result;
	try {
		result = parseAndMerge(std::move(into), blueprintFile.stream);
	}
	catch (const ParseError& error) {
		if (!quiet) {
			std::cerr << "Failed\n";
		}
		std::cerr << STYLE_ERROR << "Error: " << aec::reset << "Parse error at "
		          << STYLE_FILENAME << blueprintFile.path.string() << aec::reset
		          << ":" << std::to_string(error.lineNumber) << " - ";
		switch (error.type) {
		case ParseError::Type::ExpectedHeader:
			std::cerr << "Expected a method or line entry header\n";
			break;
		case ParseError::Type::ExpectedMethodEntryHeader:
			std::cerr << "Expected a method entry header\n";
			break;
		case ParseError::Type::InvalidMethodEntryHeader:
			std::cerr << "Invalid method entry header\n";
			break;
		case ParseError::Type::InvalidLineEntryHeader:
			std::cerr << "Invalid line entry header\n";
			break;
		case ParseError::Type::DoubleReplacement:
			std::cerr << "Double replacement of source code line\n";
			break;
		}
		exit(1);
	}

	if (!quiet) {
		std::cerr << "Done\n";
	}

	return result;
}

std::string tryApplyBlueprint(const Blueprint& blueprint, OpenedFile& smaliFile) {
	std::ostringstream ss;
	try {
		apply(blueprint, smaliFile.stream, ss);
	}
	catch (const ApplicationError& error) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset << "Application error at "
		          << STYLE_FILENAME << smaliFile.path.string() << aec::reset
		          << ":" << std::to_string(error.lineNumber) << " - ";
		switch (error.type) {
		case ApplicationError::Type::InvalidClassHeader:
			std::cerr << "Invalid class header\n";
			break;
		case ApplicationError::Type::MethodWithoutClass:
			std::cerr << "Encountered a method before any class header\n";
			break;
		case ApplicationError::Type::InvalidMethodHeader:
			std::cerr << "Invalid method header\n";
			break;
		case ApplicationError::Type::InvalidLocalRegisterCount:
			std::cerr << "Invalid local register count\n";
			break;
		case ApplicationError::Type::UnclosedMethod:
			std::cerr << "Unclosed method\n";
		}
		exit(1);
	}
	return ss.str();
}

OpenedFile openFile(const std::filesystem::path& path, std::ios_base::openmode mode) {
	return OpenedFile{fstreamFromPath(path, mode), path};
}

std::fstream fstreamFromPath(const std::filesystem::path& path, std::ios_base::openmode mode) {
	std::fstream fs(path, mode);
	if (!fs.is_open()) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset << "could not open file "
		          << STYLE_FILENAME << path.string() << aec::reset << "\n";
		exit(1);
	}
	return fs;
}

void tryCreateDirectoryPath(const std::filesystem::path& path) {
	try {
		std::filesystem::create_directories(path);
	}
	catch (const std::filesystem::filesystem_error& error) {
		std::cerr << STYLE_ERROR << "Error: " << aec::reset
		          << "could not create directory "
		          << STYLE_FILENAME << path.string() << aec::reset << "\n";
		exit(1);
	}
}

[[noreturn]]
void exitBadUsage() {
	std::cerr << "\nUse \"" << programName << " --help\" for usage information\n";
	exit(1);
}
